package com.teamvoy.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamvoy.model.Order;
import com.teamvoy.model.dto.ItemDto;
import com.teamvoy.model.dto.OrderDto;
import com.teamvoy.repository.OrderRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Example;
import util.TestUtils;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

  @Mock
  private OrderRepository orderRepository;

  @Mock
  private ObjectMapper objectMapper;

  @Mock
  private ItemServiceImpl itemService;

  @InjectMocks
  private OrderServiceImpl orderService;

  private final Order expectedOrder = TestUtils.createOrder();
  private final ItemDto expectedItemDto = TestUtils.createItemDto();
  private final OrderDto expectedOrderDto = TestUtils.createOrderDto();

  @Test
  public void save() {
    Example<Order> expectedOrderExample;
    List<ItemDto> items = new ArrayList<>();
    when(objectMapper.convertValue(expectedOrderDto, Order.class)).thenReturn(expectedOrder);
    lenient().when(orderRepository.getOne(anyLong())).thenReturn(expectedOrder);
    expectedOrderExample = Example.of(expectedOrder);
    when(orderRepository.exists(expectedOrderExample))
        .thenReturn(false);
    items.add(expectedItemDto);
    when(itemService.findAllById(anyList())).thenReturn(items);
    final boolean isExists = orderRepository.exists(expectedOrderExample);
    orderService.save(expectedOrderDto);
    assertThat(isExists).isNotNull();
    assertThat(isExists).isFalse();
    verify(orderRepository, times(1)).save(expectedOrder);
  }

  @Test
  public void getOrderById() {
    when(objectMapper.convertValue(expectedOrder, OrderDto.class))
        .thenReturn(expectedOrderDto);
    when(orderRepository.findById(anyLong()))
        .thenReturn(Optional.of(expectedOrder));
    OrderDto actualOrderDto = orderService.getOrderById(anyLong());
    assertThat(actualOrderDto).isNotNull();
    Assertions.assertThat(expectedOrder.getQuantity()).isEqualTo(actualOrderDto.getQuantity());
    Assertions.assertThat(expectedOrder.getTotalPrice()).isEqualTo(actualOrderDto.getTotalPrice());
    Assertions.assertThat(expectedOrder.getItems().get(0).getPrice())
        .isEqualTo(actualOrderDto.getItems().get(0).getPrice());
    Assertions.assertThat(expectedOrder.getItems().get(0).getBrand())
        .isEqualTo(actualOrderDto.getItems().get(0).getBrand());
    Assertions.assertThat(expectedOrder.getItems().get(0).getProductType())
        .isEqualTo(actualOrderDto.getItems().get(0).getProductType());
    verify(orderRepository, times(1)).findById(anyLong());
  }

  @Test
  public void deleteNotValidOrders() {
    List<Order> orders = new ArrayList<>();
    orders.add(expectedOrder);
    when(orderRepository.findAllByCreationDateLessThan(any(LocalDateTime.class)))
        .thenReturn(orders);
    orderService.deleteNotValidOrders();
    verify(orderRepository, times(1))
        .findAllByCreationDateLessThan(any(LocalDateTime.class));
    verify(orderRepository, times(1)).deleteAll(anyList());
  }
}
