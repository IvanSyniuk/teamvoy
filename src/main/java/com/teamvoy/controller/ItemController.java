package com.teamvoy.controller;

import com.teamvoy.model.dto.ItemDto;
import com.teamvoy.service.interfaces.ItemService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/item")
@AllArgsConstructor
public class ItemController {

  private final ItemService itemService;

  @PostMapping("/create")
  public ResponseEntity<?> createItem(@RequestBody final ItemDto item) {
    itemService.save(item);
    return ResponseEntity.status(201).build();
  }

  @GetMapping("/{id}")
  public ResponseEntity<ItemDto> getItemById(@PathVariable final Long id) {
    return ResponseEntity.ok(itemService.getItemById(id));
  }

  @GetMapping("/all")
  public ResponseEntity<List<ItemDto>> getAllRequestedItemsByMinPrice(
      @RequestParam final String productType, @RequestParam final String brand) {
    return ResponseEntity.ok(itemService.findAllRequestedItemsByMinPrice(productType, brand));
  }

}
