package com.teamvoy.service.interfaces;

import com.teamvoy.model.Item;
import com.teamvoy.model.dto.ItemDto;
import java.util.List;

public interface ItemService {

  void save(final ItemDto item);

  void saveAll(final List<ItemDto> items);

  ItemDto getItemById(final Long id);

  List<ItemDto> findAllRequestedItems(final String productType, final String brand);

  List<ItemDto> findAllRequestedItemsByMinPrice(final String productType, final String brand);

  List<ItemDto> findAllById(final List<Long> list);
}
