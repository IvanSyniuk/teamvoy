package com.teamvoy.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamvoy.exception.ItemNotFoundException;
import com.teamvoy.model.Item;
import com.teamvoy.model.dto.ItemDto;
import com.teamvoy.repository.ItemRepository;
import com.teamvoy.service.interfaces.ItemService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ItemServiceImpl implements ItemService {

  private final ItemRepository itemRepository;
  private final ObjectMapper objectMapper;

  @Override
  public void save(final ItemDto item) {
    itemRepository.save(objectMapper.convertValue(item, Item.class));
  }

  @Override
  public void saveAll(final List<ItemDto> items) {
    List<Item> itemsEntity = items.stream().map(itemDto ->
        objectMapper.convertValue(itemDto, Item.class)).collect(Collectors.toList());
    itemRepository.saveAll(itemsEntity);
  }

  @Override
  public List<ItemDto> findAllById(final List<Long> list) {
    return itemRepository.findAllById(list).stream()
        .map(item -> objectMapper.convertValue(item, ItemDto.class)).collect(Collectors.toList());
  }

  @Override
  public ItemDto getItemById(final Long id) {
    return objectMapper.convertValue(itemRepository
        .findById(id).orElseThrow(ItemNotFoundException::new), ItemDto.class);
  }

  @Override
  public List<ItemDto> findAllRequestedItems(final String productType, final String brand) {
    return itemRepository.findAllByProductTypeOrBrand(productType, brand).stream()
        .map(item -> objectMapper.convertValue(item, ItemDto.class)).collect(Collectors.toList());
  }

  @Override
  public List<ItemDto> findAllRequestedItemsByMinPrice(final String productType,
      final String brand) {
    if (itemRepository.findAllByLowestPrice(productType, brand).isEmpty()) {
      return findAllRequestedItems(productType, brand);
    } else {
      return itemRepository.findAllByLowestPrice(productType, brand).stream()
          .map(item -> objectMapper.convertValue(item, ItemDto.class)).collect(
              Collectors.toList());
    }
  }

}