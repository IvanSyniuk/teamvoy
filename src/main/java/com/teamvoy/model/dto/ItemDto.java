package com.teamvoy.model.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class ItemDto {

  private Long id;

  private String productType;

  private String brand;

  private BigDecimal price;
}
