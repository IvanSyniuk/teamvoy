package com.teamvoy.controller;

import com.teamvoy.model.dto.OrderDto;
import com.teamvoy.service.interfaces.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
@AllArgsConstructor
public class OrderController {

  private final OrderService orderService;

  @PostMapping("/create")
  public ResponseEntity<?> createOrder(@RequestBody final OrderDto order) {
    orderService.save(order);
    return ResponseEntity.status(201).build();
  }

  @GetMapping("/{id}")
  public ResponseEntity<OrderDto> getOrderById(@PathVariable final Long id) {
    return ResponseEntity.ok(orderService.getOrderById(id));
  }
}
