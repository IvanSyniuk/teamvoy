package com.teamvoy.service;

import com.teamvoy.service.interfaces.OrderService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class OrderScheduler {

  private final OrderService orderService;

  public OrderScheduler(final OrderService orderService) {
    this.orderService = orderService;
  }

  @Scheduled(cron = "* 0/10 * * * *")
  public void checkValidOrder() {
    orderService.deleteNotValidOrders();
  }
}
