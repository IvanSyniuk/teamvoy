package com.teamvoy.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamvoy.model.dto.OrderDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import util.TestUtils;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)

public class OrderControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private OrderController orderController;

  private final OrderDto expectedOrderDto = TestUtils.createOrderDto();

  @Test
  public void createOrder() throws Exception {
    when(orderController.createOrder(any(OrderDto.class)))
        .thenReturn(ResponseEntity.status(201).build());
    mockMvc
        .perform(post("/order/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(expectedOrderDto))
            .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isCreated())
        .andReturn();
    verify(orderController, times(1)).createOrder(any(OrderDto.class));
  }


  @Test
  public void getOrderById() throws Exception {
    given(orderController.getOrderById(anyLong()))
        .willReturn(ResponseEntity.ok(expectedOrderDto));
    mockMvc
        .perform(get("/order/{id}", 150)
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(content().string(objectMapper.writeValueAsString(expectedOrderDto)))
        .andExpect(jsonPath("$.totalPrice").value(expectedOrderDto.getTotalPrice()))
        .andExpect(jsonPath("$.quantity").value(expectedOrderDto.getQuantity()))
        .andExpect(jsonPath("$.items", hasSize(1)))
        .andReturn();
    verify(orderController, times(1)).getOrderById(anyLong());
  }
}