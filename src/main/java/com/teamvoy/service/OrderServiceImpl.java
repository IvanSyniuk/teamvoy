package com.teamvoy.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamvoy.exception.ItemNotFoundException;
import com.teamvoy.exception.OrderIsAlreadyExistsException;
import com.teamvoy.exception.OrderNotFoundException;
import com.teamvoy.model.Order;
import com.teamvoy.model.dto.ItemDto;
import com.teamvoy.model.dto.OrderDto;
import com.teamvoy.repository.OrderRepository;
import com.teamvoy.service.interfaces.ItemService;
import com.teamvoy.service.interfaces.OrderService;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:order.yml")
public class OrderServiceImpl implements OrderService {

  @Value("${order-expiration}")
  private long EXPIRE_ORDER;

  private final OrderRepository orderRepository;
  private final ItemService itemService;
  private final ObjectMapper objectMapper;

  public OrderServiceImpl(final OrderRepository orderRepository,
      final ItemService itemService,
      final ObjectMapper objectMapper) {
    this.orderRepository = orderRepository;
    this.itemService = itemService;
    this.objectMapper = objectMapper;
  }

  @Override
  public void save(final OrderDto order) {
    List<ItemDto> items = itemService.findAllById(order.getItems().stream()
        .map(ItemDto::getId).collect(Collectors.toList()));
    order.setTotalPrice(items.stream()
        .map(ItemDto::getPrice).reduce(new BigDecimal(0), BigDecimal::add));
    order.setQuantity(order.getItems().size());
    order.setItems(null);
    order.setCreationDate(LocalDateTime.now());
    Order orderEntity = objectMapper.convertValue(order, Order.class);
    Example<Order> orderExample = Example.of(orderEntity);
    if (orderRepository.exists(orderExample)) {
      throw new OrderIsAlreadyExistsException();
    }
    if (items.isEmpty()) {
      throw new ItemNotFoundException();
    }
    orderRepository.save(orderEntity);
    itemService.saveAll(items);
  }

  @Override
  public OrderDto getOrderById(final Long id) {
    return objectMapper.convertValue(
        orderRepository.findById(id).filter(order -> !isOrderExpired(order.getCreationDate()))
            .orElseThrow(OrderNotFoundException::new), OrderDto.class);
  }

  private boolean isOrderExpired(final LocalDateTime orderCreationDate) {
    final LocalDateTime now = LocalDateTime.now();
    Duration diff = Duration.between(now, orderCreationDate);
    return diff.toMinutes() >= EXPIRE_ORDER;
  }

  @Override
  public void deleteNotValidOrders() {
    orderRepository.deleteAll(orderRepository
        .findAllByCreationDateLessThan(LocalDateTime.now().minusMinutes(EXPIRE_ORDER)));
  }
}
