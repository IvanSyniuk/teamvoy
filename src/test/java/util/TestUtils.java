package util;

import com.teamvoy.model.Item;
import com.teamvoy.model.Order;
import com.teamvoy.model.dto.ItemDto;
import com.teamvoy.model.dto.OrderDto;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TestUtils {

  public static Item createItem(){
    Item item = new Item();
    item.setId(343L);
    item.setBrand("brand");
    item.setPrice(new BigDecimal(444));
    item.setProductType("type");
    return item;
  }

  public static Order createOrder(){
    List<Item> items = new ArrayList<>();
    items.add(createItem());
    Order order = new Order();
    order.setId(344L);
    order.setQuantity(1);
    order.setTotalPrice(new BigDecimal(444));
    order.setItems(items);
    order.setCreationDate(LocalDateTime.now().minusMinutes(10));
    return order;
  }

  public static ItemDto createItemDto(){
    ItemDto item = new ItemDto();
    item.setId(343L);
    item.setBrand("brand");
    item.setPrice(new BigDecimal(444));
    item.setProductType("type");
    return item;
  }

  public static OrderDto createOrderDto(){
    List<ItemDto> items = new ArrayList<>();
    items.add(createItemDto());
    OrderDto order = new OrderDto();
    order.setId(344L);
    order.setQuantity(1);
    order.setTotalPrice(new BigDecimal(444));
    order.setItems(items);
    order.setCreationDate(LocalDateTime.now().minusMinutes(10));
    return order;
  }
}
