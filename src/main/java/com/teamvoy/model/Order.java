package com.teamvoy.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Entity(name = "orders")
@Table(name = "orders")
@Data
public class Order {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "total_price")
  private BigDecimal totalPrice;

  @Column(name = "creation_date")
  private LocalDateTime creationDate;

  @Column(name = "quantity")
  private int quantity;

  @OneToMany(mappedBy = "order", cascade = {CascadeType.PERSIST})
  @JsonManagedReference
  private List<Item> items = new ArrayList<>();

}
