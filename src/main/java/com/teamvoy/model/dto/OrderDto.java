package com.teamvoy.model.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;

@Data
public class OrderDto {

  private Long id;

  private BigDecimal totalPrice;

  private LocalDateTime creationDate;

  private int quantity;

  private List<ItemDto> items;
}
