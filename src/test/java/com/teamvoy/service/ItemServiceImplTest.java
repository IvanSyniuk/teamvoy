package com.teamvoy.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamvoy.model.Item;
import com.teamvoy.model.dto.ItemDto;
import com.teamvoy.model.dto.OrderDto;
import com.teamvoy.repository.ItemRepository;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Example;
import util.TestUtils;

@RunWith(MockitoJUnitRunner.class)
public class ItemServiceImplTest {

  @Mock
  private ItemRepository itemRepository;

  @Mock
  private ObjectMapper objectMapper;

  @InjectMocks
  private ItemServiceImpl itemService;


  private final Item expectedItem = TestUtils.createItem();
  private final ItemDto expectedItemDto = TestUtils.createItemDto();
  private final OrderDto expectedOrderDto = TestUtils.createOrderDto();

  @Test
  public void save() {
    Example<Item> expectedItemExample;
    when(objectMapper.convertValue(expectedItemDto, Item.class)).thenReturn(expectedItem);
    lenient().when(itemRepository.getOne(anyLong())).thenReturn(expectedItem);
    expectedItemExample = Example.of(expectedItem);
    when(itemRepository.exists(expectedItemExample))
        .thenReturn(false);
    final boolean isExists = itemRepository.exists(expectedItemExample);
    itemService.save(expectedItemDto);
    assertThat(isExists).isNotNull();
    assertThat(isExists).isFalse();
    verify(itemRepository, times(1)).save(expectedItem);
  }

  @Test
  public void saveAll() {
    itemService.saveAll(expectedOrderDto.getItems());
    verify(itemRepository, times(1)).saveAll(anyList());
  }

  @Test
  public void getItemById() {
    when(objectMapper.convertValue(expectedItem, ItemDto.class))
        .thenReturn(expectedItemDto);
    when(itemRepository.findById(anyLong()))
        .thenReturn(Optional.of(expectedItem));
    ItemDto actualItemDto = itemService.getItemById(anyLong());
    assertThat(actualItemDto).isNotNull();
    Assertions.assertThat(expectedItem.getBrand()).isEqualTo(actualItemDto.getBrand());
    Assertions.assertThat(expectedItem.getPrice()).isEqualTo(actualItemDto.getPrice());
    Assertions.assertThat(expectedItem.getProductType()).isEqualTo(actualItemDto.getProductType());
    verify(itemRepository, times(1)).findById(anyLong());
  }

  @Test
  public void findAllRequestedItems() {
    when(itemRepository.findAllByProductTypeOrBrand(anyString(), anyString()))
        .thenReturn(Lists.list(expectedItem));
    when(objectMapper.convertValue(expectedItem, ItemDto.class))
        .thenReturn(expectedItemDto);
    List<ItemDto> actualItems = itemService.findAllRequestedItems(anyString(), anyString());
    Assertions.assertThat(actualItems).isNotNull();
    verify(itemRepository, times(1))
        .findAllByProductTypeOrBrand(anyString(), anyString());
  }

  @Test
  public void findAllRequestedItemsByMinPrice() {
    when(itemRepository.findAllByLowestPrice(anyString(), anyString()))
        .thenReturn(Lists.list(expectedItem));
    when(objectMapper.convertValue(expectedItem, ItemDto.class))
        .thenReturn(expectedItemDto);
    List<ItemDto> actualItems =
        itemService.findAllRequestedItemsByMinPrice(anyString(), anyString());
    Assertions.assertThat(actualItems).isNotNull();
    verify(itemRepository, times(2))
        .findAllByLowestPrice(anyString(), anyString());
  }
}