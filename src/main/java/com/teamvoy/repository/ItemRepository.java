package com.teamvoy.repository;

import com.teamvoy.model.Item;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

  List<Item> findAllByProductTypeOrBrand(String productType, String brand);

  @Query("SELECT i FROM items i  WHERE i.productType = ?1 and i.brand = ?2 "
      + "and i.price in (SELECT MIN(i.price) From items i where i.productType = ?1 and i.brand = ?2)")
  List<Item> findAllByLowestPrice(String productType, String brand);
}
