package com.teamvoy.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teamvoy.model.dto.ItemDto;
import java.util.List;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import util.TestUtils;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class ItemControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private ItemController itemController;

  private final ItemDto expectedItemDto = TestUtils.createItemDto();

  @Test
  public void createItem() throws Exception {
    when(itemController.createItem(any(ItemDto.class)))
        .thenReturn(ResponseEntity.status(201).build());
    mockMvc
        .perform(post("/item/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(expectedItemDto))
            .accept(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isCreated())
        .andReturn();
    verify(itemController, times(1)).createItem(any(ItemDto.class));
  }

  @Test
  public void getItemById() throws Exception {
    given(itemController.getItemById(anyLong()))
        .willReturn(ResponseEntity.ok(expectedItemDto));
    mockMvc
        .perform(get("/item/{id}", 150)
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(content().string(objectMapper.writeValueAsString(expectedItemDto)))
        .andExpect(jsonPath("$.price").value(expectedItemDto.getPrice()))
        .andExpect(jsonPath("$.brand").value(expectedItemDto.getBrand()))
        .andExpect(jsonPath("$.productType").value(expectedItemDto.getProductType()))
        .andReturn();
    verify(itemController, times(1)).getItemById(anyLong());
  }

  @Test
  public void getAllRequestedItemsByMinPrice() throws Exception {
    List<ItemDto> expectedListItemDto = Lists.list(expectedItemDto);
    given(itemController.getAllRequestedItemsByMinPrice(anyString(), anyString()))
        .willReturn(ResponseEntity.ok(expectedListItemDto));
    mockMvc
        .perform(get("/item/all")
            .param("productType", anyString())
            .param("brand", anyString())
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(content().string(objectMapper.writeValueAsString(expectedListItemDto)))
        .andExpect(jsonPath("$[0].price").value(expectedItemDto.getPrice()))
        .andExpect(jsonPath("$[0].brand").value(expectedItemDto.getBrand()))
        .andExpect(jsonPath("$[0].productType").value(expectedItemDto.getProductType()))
        .andReturn();
    verify(itemController, times(1)).getAllRequestedItemsByMinPrice(anyString(), anyString());
  }
}