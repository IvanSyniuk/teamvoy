package com.teamvoy.service.interfaces;

import com.teamvoy.model.dto.OrderDto;

public interface OrderService {

  void save(final OrderDto order);

  OrderDto getOrderById(final Long id);

  void deleteNotValidOrders();
}
